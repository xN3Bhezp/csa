import os
from os import path
import secrets
import json
import shutil
import hashlib
from checksumdir import dirhash
import cryptography
import py7zr
from cryptography.fernet import Fernet
import base64

# 1. Users can create, read, write, delete, rename files 				CHECK
# 2. The file system should support directories							CHECK
# 3. Users should be able to restrict access to files and directories, 
#    which also requires the file system to name users 					CHECK
# 4. File and directory names should be treated as confidential			CHECK
# 5. Users should not be able to edit files or 
#    directories without being detected.								CHECK
# SOLUTION: checksumdir module
# 6. It should be possible for each file to specify 
# the users who can read and write to that file.						CHECK
# SOLUTION: fix the functions to include permissions
# 7. Encryption															CHECK
# 8. Implente Server Client task 


version = "0.4 (Encryption)"

greeter = f"""\n
This is version {version} of EddiFS."""

userbanner = """ 
Select number for action: 
1. Create File
2. List Files
3. Rename File
4. Read File
5. Delete File
6. Edit permissions
7. Create user
8. List users
9. Delete user
h. Help
q. Exit application
"""

def createUser():
	name = input("Create user> ")
	
	# Create user dir and key
	dirname = secrets.token_urlsafe(16)
	keyname = secrets.token_urlsafe()
	
	# Update dictionary	
	userlist[name] = {}
	userlist[name]['dirname'] = dirname
	#userlist[name]['key'] = keyname
	userlist[name]['files'] = {}
	
	# Create user home dir, dummy content and zip it
	os.mkdir(f"users/{dirname}")
	dummy = open(f"users/{dirname}/index", "w")
	dummy.write("index")
	dummy.close()
	shutil.make_archive(f"{name}", "zip", f"users/{dirname}", ".", dry_run=False)
	
		
	# Create user key and encrypt user folder
	keyname = Fernet.generate_key()
	with open(f"{name}.zip", "rb") as f:
		data = f.read()
		fernet = Fernet(keyname)
		encrypted = fernet.encrypt(data)
	
	placeholder = f"{name}.zip.enc"
	with open(placeholder, "wb") as f:
		f.write(encrypted)
	
	os.remove(f"{name}.zip")

	f = open(f"{name}.key", "wb")
	f.write(keyname)
	f.close()

	# Update the creds.json file
	json_file = json.dumps(userlist)
	store = open("creds.json", "w")
	store.write(json_file)
	store.close()

	# Create initial checksum
	newhash = dirhash(f"users/{dirname}", "md5")
	userlist[name]['hash'] = newhash

	print(f"User {name} and directory {dirname} created")

	shutil.rmtree(f"users/{dirname}")

def listUsers():
	if len(userlist) == 0:
		print("no users")
		return False
	else:
		for user in userlist:
			print(user)
		return True

def deleteUser():
	if len(userlist) == 0:
		print("no users")
	else:
		print("which user do you want to delete? ")
		name = input("user to delete> ")

		# Check if name is in users, if it is then delete it
		if name in userlist:
			print(name)
			dirname = userlist[name]['dirname']
			del userlist[name]
			if path.exists(f"users/{dirname}"):
				shutil.rmtree(f"users/{dirname}", ignore_errors=True)
			if path.exists(f"{name}.key"):
				os.remove(f"{name}.key")
			if path.exists(f"{name}.zip"):
				os.remove(f"{name}.zip")
			if path.exists(f"{name}.zip.enc"):
				os.remove(f"{name}.zip.enc")
			print(f"user {name} deleted.")
		else:
			print("no such user")
		towrite = json.dumps(userlist)
		store = open("creds.json", "w")
		store.write(towrite)
		store.close()


def createFile(filename, content):
	with open(f"{userdir}/{filename}", "w") as f:
		f.write(content)
	print(f"{filename} created")
	
	userlist[user]['files'][filename] = {}
	userlist[user]['files'][filename][user] = "readwrite"
	towrite = json.dumps(userlist)
	
	store = open("creds.json", "w")
	store.write(towrite)
	store.close()

def listFiles():
	print("Here are the files in your home directory: ")
	print("FILENAME:")
	
	#for file in userfiles:
	#	print(file)
	#print("\n")
	for i in os.listdir(userdir):
		if not i == "index":
			print(i)

	userfiles = userlist[user]['files']
	print("Here are the files that you are sharing:")
	print("FILENAME, USER, PERMISSIONS:")
	for key, value in userfiles.items():
		for a, b in value.items():
			if not a == user:
				print(key, a, b)
	print("\n")
	
def renameFile():
	print("Rename File: ")
	filechoice = input("which file?> ")
	for i in os.listdir(userdir):
		if i == filechoice:
			filename = input("New file name?> ")
			os.rename(f"{userdir}/{filechoice}", f"{userdir}/{filename}")
			userlist[user]['files'][filename] = {}
			userlist[user]['files'][filename][user] = "readwrite"
			del userlist[user]['files'][filechoice]
	
			print(f"{filechoice} renamed to {filename}")


def readFile():
	print("Read File: ")
	filechoice = input("which file?> ")
	for i in os.listdir(userdir):
		if i == filechoice:
			with open(f"{userdir}/{i}") as toread:
				content = toread.read()
				print(content)

def deleteFile():
	filechoice = input("which file?> ")
	for i in os.listdir(userdir):
		if i == filechoice:
			os.remove(f"{userdir}/{i}")
			if i in userlist[user]['files']:
				del userlist[user]['files'][i]
			print(f"{i} removed")

def shareFile():
	print("Share file: ")
	try:
		filechoice = input("which file?> ")
		towho = input("to who?> ")
		perms = input("with permissions (read or readwrite)?> ")
		for i in os.listdir(userdir):
			if i == filechoice:
				userlist[user]['files'][filechoice][towho] = perms
				print(f"{i} is now shared with {perms} access to user {towho}")
	except:
		print("something went wrong")

if path.exists("creds.json"):
	with open("creds.json") as creds_file:
		
		userlist = json.loads(creds_file.read())
		user = input("login: ")
		
		for entry in userlist:
			if entry == user:
				with open(f"{user}.key", "rb") as key:
					key_read = key.read()
					with open(f"{user}.zip.enc", "rb") as f:
						data = f.read()
						fernet = Fernet(key_read)
						decrypted = fernet.decrypt(data)
						with open(f"{user}.zip", "wb") as f:
							f.write(decrypted)
					
					shutil.unpack_archive(f"{user}.zip", f"users/{userlist[user]['dirname']}", "zip")
					
				with open("creds.json") as u:
					userlist = json.loads(u.read())
					userdir = os.path.abspath(f"users/{userlist[user]['dirname']}/")
					
					print(f"Welcome {user}. You are now logged in.")
					print(userbanner)
					print(greeter)

					hashvalue = dirhash(userdir, "md5")

					if not hashvalue == userlist[user]['hash']:
						print("careful, your files has been tampered with!")

					while True:
						choice = input("menu> ")
	
						if choice == "1":
							filename = input("Give a filename: ")
							content = input(f"Give content for {filename}: ")
							createFile(filename, content)
	
						if choice == "2":
							listFiles()
	
						if choice == "3":
							listFiles()
							renameFile()
	
						if choice == "4":
							listFiles()
							readFile()
	
						if choice == "5":
							listFiles()
							deleteFile()
	
						if choice == "6":
							listFiles()
							shareFile()

						if choice == "hash":
							getHash()

						if choice == "7":
							createUser()

						if choice == "8":
							listUsers()

						if choice == "9":
							deleteUser()

						if choice == "h":
							print(userbanner)

						if choice == "q":
							hashvalue = dirhash(userdir, "md5")
							userlist[user]['hash'] = hashvalue
							json_file = json.dumps(userlist)
							store = open("creds.json", "w")
							store.write(json_file)
							store.close()

							shutil.make_archive(f"{user}", "zip", f"users/{userlist[user]['dirname']}", ".", dry_run=False)
							shutil.rmtree(f"users/{userlist[user]['dirname']}")
							
							key = open(f"{user}.key", "rb")
							key_read = key.read()
							key.close()

							with open(f"{user}.zip", "rb") as f:
								data = f.read()
								fernet = Fernet(key_read)
								encrypted = fernet.encrypt(data)
							
							placeholder = f"{user}.zip.enc"
							with open(placeholder, "wb") as f:
								f.write(encrypted)
							
							os.remove(f"{user}.zip")
							
							while True:
								q = input("Do you want to share your files on a ftp server? (Y/N)\n?> ")
								q = q.upper()
								if q == "Y":
									import ftplib
									from socket import gethostbyname, gaierror
									hostname = input("Enter ftp-server hostname:\n> ")
									ftpusername = input("Enter ftp-server username:\n> ")
									if len(ftpusername) == 0:
										ftpusername = "anonymous"
									ftppassword = input("Enter ftp-server password:\n> ")
									if len(ftppassword) == 0:
										ftppassword == "anonymous"
									
									def upload(ftp, file):
										ext = os.path.splitext(file)[1]
										if ext in (".txt", ".htm", ".html"):
											ftp.storlines("STOR " + file, open(file))
										else:
											ftp.storbinary("STOR " + file, open(file, "rb"), 1024)
									try:
										gethostbyname(hostname)
										ftp = ftplib.FTP(hostname)
										ftp.login(ftpusername, ftppassword)
										upload(ftp, f"{user}.zip.enc")
										print(f"file {user}.zip.enc uploaded through ftp to {hostname}.")
										exit()
									except gaierror:
										print("Not a valid hostname, try again.")
								
								elif q == "N":
									exit()
								
								else:
									continue
else:
	print("the creds.json file was not found")
									